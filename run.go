package command

import (
	"encoding/json"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

const (
	// ArtifactNameSAST holds the default name for SAST tool report file.
	ArtifactNameSAST = "gl-sast-report.json"
	// ArtifactNameDependencyScanning holds the default name for Dependency Scanning tool report file.
	ArtifactNameDependencyScanning = "gl-dependency-scanning-report.json"
	// ArtifactNameContainerScanning holds the default name for Container Scanning tool report file.
	ArtifactNameContainerScanning = "gl-container-scanning-report.json"
	// ArtifactNameSecretDetection holds the default name for Secret Detection tool report file.
	ArtifactNameSecretDetection = "gl-secret-detection-report.json"

	// EnvVarTargetDir is the env var (without prefix) for setting the analyzer target directory.
	EnvVarTargetDir = "ANALYZER_TARGET_DIR"
	// EnvVarArtifactDir is the env var (without prefix) for setting the analyzer artifacts directory.
	EnvVarArtifactDir = "ANALYZER_ARTIFACT_DIR"
	// EnvVarCIProjectDir is the env var that holds the project dir path
	// and usually is propagated from GitLab Runner. It's used as
	// default value for target directory and artifact directory in case
	// EnvVarTargetDir or EnvVarArtifactDir are not set.
	EnvVarCIProjectDir = "CI_PROJECT_DIR"

	flagTargetDir   = "target-dir"
	flagArtifactDir = "artifact-dir"
)

// Run returns a cli sub-command that implements the full analyzer execution cycle.
func Run(cfg Config) *cli.Command {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:    flagTargetDir,
			Usage:   "Target directory",
			EnvVars: []string{EnvVarTargetDir, EnvVarCIProjectDir},
		},
		&cli.StringFlag{
			Name:    flagArtifactDir, // #nosec
			Usage:   "Artifact directory",
			EnvVars: []string{EnvVarArtifactDir, EnvVarCIProjectDir},
		},
	}

	flags = append(flags, cacert.NewFlags()...)
	flags = append(flags, search.NewFlags()...)
	flags = append(flags, cfg.AnalyzeFlags...)

	// Artifact name defaults to the name of SAST artifacts
	// to ensure backward compatibility with existing SAST analyzers.
	// There's no NewConfig() function so this is the best place to put it.
	artifactName := cfg.ArtifactName
	if artifactName == "" {
		artifactName = ArtifactNameSAST
	}

	// HACK: guess prefix for env var based on report file name
	var envPrefix = "SAST_"
	if artifactName == ArtifactNameDependencyScanning {
		envPrefix = "DS_"
	}
	flags = append(flags, pathfilter.MakeFlags(envPrefix)...)

	return &cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			startTime := report.ScanTime(time.Now())
			// no args
			if c.Args().Present() {
				if err := cli.ShowSubcommandHelp(c); err != nil {
					return err
				}
				return errInvalidArgs
			}

			// import CA bundle
			if err := cacert.Import(c, cfg.CACertImportOptions); err != nil {
				return err
			}

			// parse excluded paths
			filter, err := pathfilter.NewFilter(c)
			if err != nil {
				return err
			}

			// search directory
			log.Info("Detecting project")
			root, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			// search
			searchOpts := search.NewOptions(c)
			matchPath, err := search.New(cfg.Match, searchOpts).Run(root)
			if err != nil {
				if e, ok := err.(search.ErrNotFound); ok {
					log.Warn(e)
					return nil
				}

				return err
			}
			log.Infof("Found project in %s\n", matchPath)

			// target directory
			var target string
			if cfg.AnalyzeAll {
				// analyze the root directory
				target = root
			} else {
				// analyze the directory where there was a match
				target, err = filepath.Abs(matchPath)
				if err != nil {
					return err
				}
			}

			// analyze
			log.Info("Running analyzer")
			f, err := cfg.Analyze(c, target)
			if err != nil {
				return err
			}
			defer f.Close()

			// relative path of the target directory
			rel, err := filepath.Rel(root, target)
			if err != nil {
				return err
			}

			// convert
			log.Info("Creating report")
			newReport, err := cfg.Convert(f, rel)
			if err != nil {
				return err
			}
			endTime := report.ScanTime(time.Now())
			newReport.Scan.Scanner = cfg.Scanner
			newReport.Scan.Type = cfg.ScanType
			newReport.Scan.StartTime = &startTime
			newReport.Scan.EndTime = &endTime
			newReport.Scan.Status = report.StatusSuccess

			// filter paths, sort
			newReport.ExcludePaths(filter.IsExcluded)
			newReport.FilterDisabledRules(filepath.Join(root, newReport.Config.Path), newReport.Analyzer)
			newReport.Sort()

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), artifactName)
			artifactFile, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644) // #nosec
			if err != nil {
				return err
			}
			enc := json.NewEncoder(artifactFile)
			enc.SetIndent("", "  ")
			if err := enc.Encode(newReport); err != nil {
				cerr := f.Close()
				if cerr != nil {
					return cerr
				}
				return err
			}
			return f.Close()
		},
	}
}
