# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The release dates can be found on the [releases page](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases).

## v1.1.0
### Changed

- Update subcommands to rely on `analyzers/report/v2`, switching `Issue` struct to `Vulnerability` (!3)

## v1.0.1
### Changed

- Safe handle command file closures (!2)

## v1.0.0
### Added

- Add command package for secure analyzer support (!1)
 
