# Analyzers Command Library
[![](https://gitlab.com/gitlab-org/security-products/analyzers/command/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/command/commits/master)
[![](https://gitlab.com/gitlab-org/security-products/analyzers/command/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/jobs)

This repository contains Go modules for implementing a command interface for GitLab Secure analyzers.

## How to use the library

Analyzer relies on the `command` Go package to implement
a command line that implements these sub-commands:

- `search` searches for a project that is supported by the analyzer.
- `analyze` performs the analysis in a given directory.
- `convert` converts the output to a `gl-sast-report.json` artifact.
- `run` performs all the previous steps consecutively.

All you need to do is to implement:
- a match function that implements [`command.MatchFunc`](search.go)
- a conversion function that implements [`command.ConvertFunc`](convert.go)
- an [analyze function](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/blob/3eb64883c3e6143ce0322187499b7a3b0795eb11/analyze.go#L15) and a function that lists the flags for the `analyze` sub-command

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
