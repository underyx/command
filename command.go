package command

import (
	"github.com/urfave/cli/v2"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

// Config struct describes the required implementation details an analyzer
// must provide to generate appropriate CLI commands for it to expose.
type Config struct {
	ArtifactName        string                // Name of the generated artifact
	Match               search.MatchFunc      // Match is a function that detects a compatible project.
	Analyze             AnalyzeFunc           // Analyze is a function that performs the analysis where a project was detected.
	AnalyzeFlags        []cli.Flag            // AnalyzeFlags is a set command line options used by the analyze function (optional).
	AnalyzeAll          bool                  // AnalyzeAll instructs the run command to analyze the root directory (false by default).
	Convert             ConvertFunc           // Convert is a function that turns the analyzer output into a compatible artifact.
	CACertImportOptions cacert.ImportOptions  // CACertImportOptions are options for the import of CA certificates.
	Scanner             report.ScannerDetails // Scanner contains detailed information about the scanner
	ScanType            report.Category       // ScanType is the type of the scan (container_scanning, dependency_scanning, dast, sast)
}

// NewCommands function creates a slice of CLI command structs
// that contains all required analyzer commands: run, search, analyze, convert.
func NewCommands(cfg Config) []*cli.Command {
	if cfg.Match == nil {
		panic("Match function not defined")
	}
	if cfg.Analyze == nil {
		panic("Analyze function not defined")
	}
	if cfg.AnalyzeFlags == nil {
		panic("Analyze flags not defined")
	}
	if cfg.Convert == nil {
		panic("Convert function not defined")
	}

	return []*cli.Command{
		Run(cfg),
		Search(cfg),
		Analyze(cfg),
		Convert(cfg),
	}
}

// NewApp creates a new cli app with the given details
func NewApp(scanner report.ScannerDetails) *cli.App {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = scanner.Version
	app.Authors = []*cli.Author{{Name: scanner.Vendor.Name}}
	app.Usage = scanner.String()

	log.SetFormatter(&logutil.Formatter{Project: scanner.Name})
	log.Info(scanner)

	return app
}
