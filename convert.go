package command

import (
	"encoding/json"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

// ConvertFunc is a type for a function that parses the analyzer binary raw output
// and converts it into the report data structure provided by the library.
type ConvertFunc func(input io.Reader, prependPath string) (*report.Report, error)

const flagPrependPath = "prepend-path"

// Convert returns a cli sub-command that converts the analyzer output into an artifact.
func Convert(cfg Config) *cli.Command {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:  flagPrependPath,
			Usage: "Path prepended to paths of affected files",
			Value: ".",
		},
	}

	return &cli.Command{
		Name:      "convert",
		Aliases:   []string{"c"},
		Usage:     "Convert analyzer output to a compatible artifact",
		ArgsUsage: "<input>",
		Flags:     append(cacert.NewFlags(), flags...),
		Action: func(c *cli.Context) error {
			// check args
			if c.Args().Len() != 1 {
				if err := cli.ShowSubcommandHelp(c); err != nil {
					return err
				}
				return errInvalidArgs
			}

			// import CA bundle
			if err := cacert.Import(c, cfg.CACertImportOptions); err != nil {
				return err
			}

			// open input file
			input := c.Args().First()
			reader, err := os.Open(input) // #nosec
			if err != nil {
				return err
			}
			defer func() {
				if err := reader.Close(); err != nil {
					log.Error(err)
				}
			}()

			// convert output to report
			log.Info("Creating report")
			report, err := cfg.Convert(reader, c.String(flagPrependPath))
			if err != nil {
				return err
			}

			report.Scan.Scanner = cfg.Scanner
			report.Scan.Type = cfg.ScanType

			// write JSON output
			enc := json.NewEncoder(c.App.Writer)
			enc.SetIndent("", "  ")
			return enc.Encode(report)
		},
	}
}
